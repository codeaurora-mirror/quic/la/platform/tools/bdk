#
# Copyright 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#
# This file sets up the proper environment and runs gdbclient.py.
#
# Usage:
#  make -f path/to/bdk/tools/bdk/build/wrap-gdb.mk gdbclient \
#       BDK_PATH=... \
#       PRODUCT_BDK=... \
#       PRODUCT_NAME=... \
#       BUILDTYPE=... \
#       ADB_PATH=... \
#       GDBCLIENT_ARGS=...
#
# Many of these variables could be automatically derived here, but this is only
# really meant to be used from our Python scripts which have easier access to
# the variables, so passing them in is simpler.
#
# We use a makefile here instead of doing this work directly in Python because
# setting up the environment involves interacting with the Android build system,
# and it's cleaner if we can keep all the build logic confined to our makefiles.
#

include $(BDK_PATH)/tools/bdk/build/wrap-common.mk

.PHONY: $(MAKECMDGOALS)

gdbclient:
	$(call run-gdbclient,$(PRODUCT_BDK),$(PRODUCT_NAME),$(BUILDTYPE),$(ADB_PATH),$(GDBCLIENT_ARGS))
