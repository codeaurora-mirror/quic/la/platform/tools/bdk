#
# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""Functions and globals to check and report the status of BSPs."""

# Statuses should be numbered low to high, where a higher status
# of a single package within a BSP dominates a lower status
# for determining the overall health of that BSP.
INSTALLED = 0
UNRECOGNIZED = 1
INCORRECT_VERSION = 2
NOT_INSTALLED = 3
INVALID = 4

def StatusString(status):
  """Human readable version of a status."""
  if status == INSTALLED:
    return "Installed"
  elif status == UNRECOGNIZED:
    return "Unrecognized"
  elif status == NOT_INSTALLED:
    return "Not Installed"
  elif status == INCORRECT_VERSION:
    return "Incorrect Version"
  elif status == INVALID:
    return "Invalid State"
  else:
    return "Unknown State"
