#
# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""Reads the manifest file into a dictionary."""

import json

import manifest
from core import util

def Read(manifest_file, bdk=None):
  """Reads in a manifest from a json file.

  Args:
    manifest_file - the path to the file containing the manifest json.
    bdk - (optional). Path to the BDK root to use for BSP operations.

  Returns:
    A manifest.Manifest object based on the file passed in.

  Raises:
    IOError - if there are issues opening the file.
    ValueError - if the specified file does not parse as valid json,
      or is not a valid manifest for reasons other than KeyErrors.
    KeyError - if a required manifest key is missing.
  """

  manifest_dic = {}
  try:
    with open(manifest_file) as f:
      manifest_dic = json.load(f)
  except IOError as e:
    raise IOError, 'Unable to open bsp manifest file {0}: {1}.'.format(manifest_file, e)
  except ValueError as e:
    raise ValueError, ('Could not parse json in '
                       'bsp manifest file {0}: {1}').format(manifest_file, e)

  try:
    result = manifest.Manifest.FromDict(manifest_dic, bdk)
  except KeyError as e:
    raise KeyError, 'Missing value in bsp manifest file {0}: {1}.'.format(manifest_file, e)
  except ValueError as e:
    raise ValueError, 'Error in bsp manifest file {0}: {1}.'.format(manifest_file, e)

  return result
