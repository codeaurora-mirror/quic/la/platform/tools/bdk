#
# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""Brunch CLI tool"""

import os
import signal
import sys

from cli import climanager

# Handle Ctrl-C.
def CtrlCHandler(sig, frame):
  """SIGINT handler.

  Handle Ctrl-C so we dont print stack traces
  """
  sys.stderr.write('Brunch killed by user')

  # Actually send the SIGINT so parent can
  # detect it.
  signal.signal(signal.SIGINT, signal.SIG_DFL)
  os.kill(os.getpid(), signal.SIGINT)
  sys.exit(1)  # Shouldn't get here

signal.signal(signal.SIGINT, CtrlCHandler)

def CommandPath(name):
  brunch_lib_path = os.path.dirname(__file__)
  path = os.path.join(brunch_lib_path, 'commands', name)
  return path

def main():
  # Create the CLI.
  brunch_lib_path = os.path.dirname(__file__)
  brunch_cli = climanager.Cli('brunch',CommandPath('root'))

  # Add command groups.
  brunch_cli.AddCommandGroup('bsp', CommandPath('bsp'))
  brunch_cli.AddCommandGroup('config', CommandPath('config'))
  brunch_cli.AddCommandGroup('product', CommandPath('product'))

  # Process the command line.
  return brunch_cli.Execute()

if __name__ == '__main__':
  try:
    sys.exit(main())
  except KeyboardInterrupt:
    CtrlCHandler(None, None)
