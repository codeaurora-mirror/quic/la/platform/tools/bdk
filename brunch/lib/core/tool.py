#
# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""Support tools execution functionality"""

import copy
import os
import string
import subprocess

from core import config
from core import util

class ToolWrapper(object):
  """Base command execution wrapper for brunch"""
  # Variables to passthrough to all command calls.
  _DEFAULT_PASSTHROUGH_ENV = [ 'http_proxy', 'https_proxy', 'ftp_proxy',
                               'rsync_proxy', 'no_proxy', 'HOME', 'USER',
                               'LANG', 'LOGNAME', 'SSH_AUTH_SOCK', 'PWD',
                               'TERM' ]
  _UNTOUCHABLE_ENV = [ 'BDK_PATH', 'PATH', 'ANDROID_PRODUCT_OUT',
                       'ANDROID_BUILD_TOP' ]

  def __init__(self, config, product_path, path):
    """Wrap a host binary, target script, or build command"""
    self._product_path = product_path
    self._tool_path = path
    self._config = config
    self._env_path = ''
    if os.environ.has_key('PATH'):
      self._env_path = os.environ['PATH']
    self._cwd = None

    self.environment = {}
    self._import_environ()
    self.environment.update({
      'BDK_PATH' : util.GetBDKPath(),
      'PATH' : string.join(filter(None, [os.path.join(util.GetDevToolsPath(), 'brunch'),
                            self._env_path]), os.pathsep),
      'ANDROID_PRODUCT_OUT' : os.path.join(self._product_path, 'out',
                                           'out-%s' % (self._config.device),
                                           'target', 'product', self._config.device),
      'ANDROID_BUILD_TOP' : os.path.join(self._product_path, 'out', '.bdk')
    })

  def _import_environ(self):
    """Walk the global environment merging in allowed variables."""
    to_import = copy.copy(self._DEFAULT_PASSTHROUGH_ENV)
    extra_vars = self._config.bdk.allowed_environ
    if extra_vars:
      to_import += extra_vars.split(' ')
    for var in to_import:
      if var in self._UNTOUCHABLE_ENV:
        print ("Cannot passthrough environment variable '{0}' "
               "as set in config/bdk/allowed_environ").format(var)
      if os.environ.has_key(var):
        self.environment[var] = os.environ[var]

  def set_cwd(self, cwd):
    self._cwd = cwd

  def path(self):
    return self._tool_path

  def exists(self):
    return os.path.isfile(self._tool_path)

  def run(self, arg_array):
    # Make sure PWD is accurate on CWD change
    if self._cwd:
      self.environment['PWD'] = os.path.abspath(self._cwd)
    try:
      ret = subprocess.call([self._tool_path] + arg_array, env=self.environment,
                            shell=False, cwd=self._cwd)
    except OSError as e:
      print "Failed to execute '{0}': {1} [{2}]".format(
        self._tool_path, e.errno, e.strerror)
      return 1
    if ret < 0:  # signal
      # Return the normal shell exit mask for being signalled.
      ret = 128 - ret
    return ret


class HostToolWrapper(ToolWrapper):
  TOOL_PATH_FMT = os.path.join('{0}', 'out', 'out-{1}',
                               'host', '{2}', 'bin', '{3}')
  DEFAULT_ARCH = 'linux-x86'
  def __init__(self, config, product_path, name, arch=DEFAULT_ARCH):
    self._tool_name = name
    self._host_arch = arch
    super(HostToolWrapper, self).__init__(config, product_path, name)
    self._tool_path = self._build_path()

  def _build_path(self):
    return self.TOOL_PATH_FMT.format(
      self._product_path, self._config.device, self._host_arch, self._tool_name)


class TargetToolWrapper(HostToolWrapper):
  TOOL_PATH_FMT = os.path.join('{0}', 'out', 'out-{1}',
                               'target', 'product', '{2}', '{3}')
  def _build_path(self):
    return self.TOOL_PATH_FMT.format(
      self._product_path, self._config.device, self._config.device, self._tool_name)
