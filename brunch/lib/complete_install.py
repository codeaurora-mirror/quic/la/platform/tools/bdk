#
# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""Completes the installation of brunch.

Sets up user options, and reports BDK download time
if the user opts in to metrics.
"""

import argparse
import sys

from core import config
from metrics.data_types import brillo_ga_hits
from metrics import send_hits

def main():
  parser = argparse.ArgumentParser(description='Install brunch.')
  parser.add_argument('-t', '--time', type=int, default=None,
                      help='the amount of time it took to download the BDK.')
  args = parser.parse_args()

  # Set up user options
  user_options = config.UserStore()
  if not user_options.complete():
    user_options.initialize()

  # Offer to add brunch to $PATH
  # TODO(arihc)(b/25951647): Whether or not we're doing this is up in the air right now.

  # Send install metrics if opted in
  if args.time and user_options.metrics_opt_in == '1':
    send_hits.SendHitAndRetries(brillo_ga_hits.InstallTiming(args.time))


if __name__ == '__main__':
  main()
