#
# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
"""A simple script to check if the user is opted in to metrics.

sys.exit is used to pass the return value because this script is
invoked by a shell script. This script is planned to become obsolete
in the near future when builds are wrapped in python."""

import sys

from core import config

def Check():
  """Gets whether or not the user is opted in to metrics.

  Returns:
    A value evaluating to True if the user is opted in,
    False otherwise.
  """
  return config.UserStore().metrics_opt_in or False

if __name__ == '__main__':
  sys.exit(int(Check()))
