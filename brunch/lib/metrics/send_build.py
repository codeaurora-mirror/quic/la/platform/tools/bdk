#
# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""An invocable script to send a build event (and retry past failed sends)."""

import sys

from data_types import brillo_ga_hits
from send_hits import SendHitAndRetries


def SendBuildAndRetries(build_type, time):
  """Sends a Brillo build event, possibly retrying previously failed sends.

  Retries past failed sends only if this send succeeds.

  Args:
    build_type: The type of build.
    time: The time the build took.
  """
  SendHitAndRetries(brillo_ga_hits.BuildTiming(build_type, time))


if __name__ == "__main__":
  if len(sys.argv) != 3:
    print "Usage: python", sys.argv[0], "[build_type] [build_time]"
    exit(1)
  SendBuildAndRetries(sys.argv[1], sys.argv[2])
