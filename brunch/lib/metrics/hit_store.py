#
# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

import sqlite3

from core import util

class Backup(object):
  """A class to back up hits that fail to send.

  Backs up hits into a sqlite DB.
  """
  def __init__(self, table='data', file_path=util.METRICS_DB):
    self._conn = None
    self._path = file_path
    self._table = table
    self._initialized = False

  def _Setup(self):
    if self._initialized: return True
    self._conn = sqlite3.connect(self._path)
    self._conn.execute('CREATE TABLE IF NOT EXISTS %s (unused)' % self._table)
    self._initialized = True

  def _AddFields(self, fields):
    """Ensures that all necessary columns in the table are present.

    This lets us store all hit types in the same table, so that when
    we extract, we don't have to scan multiple tables. Furthermore we
    don't have to write initializers for tables of different hit types.

    Args:
      fields - a list of columns the table should have.
    """
    if not self._initialized: self._Setup()
    # Get existing columns and find which expected ones are missing.
    c = self._conn.cursor()
    c.execute('PRAGMA TABLE_INFO(%s)' % self._table)
    existing = [column_info[1] for column_info in c]
    missing = [field for field in fields if not field in existing]

    # Add missing columns
    for field in missing:
      c.execute('ALTER TABLE %s ADD COLUMN %s' % (self._table, field))
    self._conn.commit()

  def Save(self, fields):
    """Saves a hit.

    Args:
      fields - a dictionary of { key, val } pairs for the hit.
    """
    if not self._initialized: self._Setup()
    # Ensure we have all the fields we need.
    self._AddFields(fields.keys())

    # Save the hit
    c = self._conn.cursor()
    keys_string = ', '.join(["'%s'" % a for a in fields.keys()])
    bind_string = ', '.join('?' * len(fields))
    values_list = map(str, fields.values())
    c.execute('INSERT INTO %s (%s) VALUES (%s)' % (self._table,
                                                   keys_string,
                                                   bind_string),
              values_list)
    self._conn.commit()

  def RetrieveAll(self):
    """Retrieves all saved hits and removes them from the DB.

    Returns:
      A list of dictionaries of { key, val } pairs. Each dictionary
    contains the data for a single hit."""
    if not self._initialized: self._Setup()
    # Make sure this happens atomically - we don't want another process
    # also retrieving hits we're about to delete.
    self._conn.isolation_level = 'EXCLUSIVE'

    # Fetch everything
    c = self._conn.cursor()
    c.execute('SELECT * FROM %s' % self._table)

    # Build the hit dictionaries
    hits = []
    keys = [column_info[0] for column_info in c.description]
    for vals in c:
      hit = { keys[i] : vals[i] for i in range(len(keys)) if (vals[i] is not None) }
      hits.append(hit)

    # Remove the rows that were retrieved
    c.execute('DELETE FROM %s' % self._table)
    self._conn.commit()

    # Clean up and return.
    self._conn.isolation_level = None
    return hits
