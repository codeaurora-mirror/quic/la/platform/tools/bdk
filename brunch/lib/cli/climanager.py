#
# Copyright (C) 2015 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

"""Brunch CLI managment"""

import argparse

import importutils
import clicommand

class CommandGroup(object):
  """A CLI Command Group"""

  def __init__(self, name, path, parent=None):
    self.name = name
    self.parent = parent
    self.path = path
    self.group_class = self.GetGroupClassType()

    if parent is None:
      self.parser = argparse.ArgumentParser(prog=name,
          description=self.group_class.__doc__)
    else:
      self.parser = parent.subparsers.add_parser(name,
          help=self.group_class.__doc__)
    self.subparsers = self.parser.add_subparsers()

    self.FindCommands()

  def AddCommandGroup(self, name, path):
    group = CommandGroup(name, path, parent=self)

  def AddCommand(self, command_type):
    name = command_type.__name__.lower()
    help_string = command_type.__doc__
    com_parser = self.subparsers.add_parser(name, description=help_string,
                                            help=help_string)
    com_parser.set_defaults(command_type=command_type)
    command_type.Args(com_parser)
    self.group_class.GroupArgs(com_parser)
    # Update the command with its parentage
    command_type.set_group(self)

  def FindTypes(self, module, find_type):
    class_types = []
    for item in module.__dict__.values():
      if issubclass(type(item), type):
        if issubclass(item, find_type):
          class_types.append(item)
    return class_types

  def FindCommands(self):
    count = 0
    # Iterate modules looking for commands
    for module in importutils.ModuleIter(self.path):
      class_types = self.FindTypes(module, clicommand.Command)
      for class_type in class_types:
        self.AddCommand(class_type)
        count = count + 1
    if count == 0:
      # TODO(leecam)(b/25951591): Change to use metric reporting Exception
      raise

  def GetGroupClassType(self):
    package = importutils.LoadPackage(self.path)
    class_types = self.FindTypes(package, clicommand.Group)
    if len(class_types) != 1:
      # TODO(leecam)(b/25951591): Change to use metric reporting Exceptions
      raise
    return class_types[0]

class Cli(object):
  """CLI managment"""

  def __init__(self, name, root_path):
    self.name = name
    self.root_path = root_path

    self.root_group = CommandGroup(name, root_path)

  def AddCommandGroup(self, name, path):
    self.root_group.AddCommandGroup(name, path)

  def Execute(self, args=None):
    args = self.root_group.parser.parse_args()

    command = args.command_type()
    result = command.Run(args)
    # Enforce the return type with a useful message.
    if type(result) != int:
      raise TypeError, 'Non-integer return code from "{0} {1}". ' \
                       'Please report this bug!'.format(
        args.command_type.group().name,
        args.command_type.__name__.lower())
    return result
